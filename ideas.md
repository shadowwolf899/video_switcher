# General
* 5 HDMI Inputs plus some extra
* 3 RCA plus some extra
* 1 or 2 VGA

* 2 HDMI outputs plus some extra
* 1 RCA plus some extra
* 1 or 2 VGA

# Converters needed
* RCA to HDMI
* VGA to HDMI
* HDMI to VGA

Nice to haves:
* HDMI to RCA
* RCA to VGA

Might consider:
* VGA to RCA

# Logic devices
* FPGA + some amount of circuitry
* Arduino + circuit
* Raspberry pi + circuit

# UI
* Web interface
* Touch screen
* IR
