# video_switcher

This is a personal project to build a device that will switch from a number of inputs to a number of outputs. It will also have built-in video converter(s) to convert from different types to others. 

It will have at least the following inputs:
* HDMI
* RCA/Composite
* VGA

It will have at least the following outputs:
* HDMI
* RCA/Composite
* VGA
